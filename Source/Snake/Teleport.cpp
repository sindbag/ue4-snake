// Fill out your copyright notice in the Description page of Project Settings.


#include "Teleport.h"

#include "SnakeBase.h"
#include "SnakeElementBase.h"

// Sets default values
ATeleport::ATeleport()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ATeleport::BeginPlay()
{
	Super::BeginPlay();
	
}

void ATeleport::Interact(AActor* Actor)
{
	const auto Snake = Cast<ASnakeBase>(Actor);
	if (IsValid(Snake))
	{
		FVector SnakeHeadLocation = Snake->SnakeElements[0]->GetActorLocation();
		switch (Snake->LastMovementDirection)
		{
			case EDirection::Up:
				SnakeHeadLocation.X *= -1;
				SnakeHeadLocation.X += Snake->ElementSize;
				break;
			case EDirection::Down:
				SnakeHeadLocation.X *= -1;
				SnakeHeadLocation.X -= Snake->ElementSize;
				break;
			case EDirection::Left:
				SnakeHeadLocation.Y *= -1;
				SnakeHeadLocation.Y -= Snake->ElementSize;
				break;
			case EDirection::Right:
				SnakeHeadLocation.Y *= -1;
				SnakeHeadLocation.Y += Snake->ElementSize;
				break;
		}
		
		Snake->SnakeElements[0]->SetActorLocation(SnakeHeadLocation);
	}
}

// Called every frame
void ATeleport::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

