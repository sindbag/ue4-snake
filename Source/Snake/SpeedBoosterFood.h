// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Food.h"
#include "SpeedBoosterFood.generated.h"

/**
 * 
 */
UCLASS()
class SNAKE_API ASpeedBoosterFood : public AFood
{
	GENERATED_BODY()

public:
	virtual void Interact(AActor* Actor) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Speed Boost")
	float SpeedBoost = 2.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Speed Boost")
	float SpeedBoostDuration = 5.0f;
};
