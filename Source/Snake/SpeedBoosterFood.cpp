// Fill out your copyright notice in the Description page of Project Settings.


#include "SpeedBoosterFood.h"
#include "SnakeBase.h"

void ASpeedBoosterFood::Interact(AActor* Actor)
{
	const auto Snake = Cast<ASnakeBase>(Actor);
	if (IsValid(Snake))
	{
		Snake->IncreaseSpeed(SpeedBoost, SpeedBoostDuration);
		TeleportToNewLocation();
	}
}
